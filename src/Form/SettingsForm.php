<?php

namespace Drupal\frontify\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Creates authorization form for frontify.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'frontify_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config('frontify.settings');

    $form['frontify_api'] = [
      '#type' => 'details',
      '#title' => $this->t('Frontify API'),
      '#open' => TRUE,
    ];

    $form['frontify_api']['url'] = [
      '#type' => 'url',
      '#title' => $this->t('URL'),
      '#default_value' => $config->get('frontify_api_url'),
      '#required' => TRUE,
      '#description' => $this->t('Url must be like https://test.frontify.com.'),
    ];

    $form['debug_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Debug'),
      '#open' => FALSE,
    ];

    $form['debug_settings']['debug_mode'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable debug mode'),
      '#description' => $this->t('Enable debug mode and dump important data via console.log()'),
      '#default_value' => $config->get('debug_mode'),
    ];
    $form = parent::buildForm($form, $form_state);
    $form['creds']['actions'] = $form['actions'];
    unset($form['actions']);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $config = $this->config('frontify.settings');
    $config->set('frontify_api_url', $form_state->getValue('url'));
    $config->set('debug_mode', $form_state->getValue('debug_mode'));

    $config->save();
    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [
      'frontify.settings',
    ];
  }

}
