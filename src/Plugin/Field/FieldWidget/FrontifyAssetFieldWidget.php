<?php

namespace Drupal\frontify\Plugin\Field\FieldWidget;

use Drupal;
use Drupal\Core\Field\Annotation\FieldWidget;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\link\Plugin\Field\FieldWidget\LinkWidget;

/**
 * Frontify asset field widget.
 *
 * @FieldWidget(
 *   id = "frontify_asset_field_widget",
 *   label = @Translation("Frontify Asset Image Url"),
 *   description = @Translation("A plaintext field for a frontify asset plus
 *   alt."), field_types = {
 *     "frontify_asset_field"
 *   }
 * )
 */
class FrontifyAssetFieldWidget extends LinkWidget {

  /**
   * Form element validation handler for the 'uri' element.
   *
   * Disallows saving inaccessible or untrusted URLs.
   */
  public static function validateUriElement($element, FormStateInterface $form_state, $form): void {
    $uri = static::getUserEnteredStringAsUri($element['#value']);
    $form_state->setValueForElement($element, $uri);
    if ($element['#required'] && !$uri) {
      $form_state->setError($element, t('Field @filedname is require!', ['@filedname' => '']));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(
    FieldItemListInterface $items,
    $delta,
    array $element,
    array &$form,
    FormStateInterface $form_state
  ): array {
    $config = Drupal::config('frontify.settings');
    $item = $items[$delta];

    $element['uri'] = [
      '#type' => 'url',
      '#default_value' => $item->uri,
      '#element_validate' => [[static::class, 'validateUriElement']],
      '#maxlength' => 2048,
      '#attributes' => [
        'class' => ['frontify-asset-link-url', 'hidden'],
      ],
    ];

    $element['frontify_preview'] = [
      '#theme' => 'image',
      '#uri' => $item->uri,
      '#alt' => '',
      '#title' => '',
      '#attributes' => [
        'class' => 'frontify-image-preview',
      ],
    ];

    $element['frontify_wrapper_overlay'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['frontify-wrapper-finder-overlay'],
      ],
    ];

    $element['frontify_wrapper_overlay']['frontify_wrapper'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['frontify-finder-wrapper'],
      ],
    ];

    $element['open'] = [
      '#type' => 'button',
      '#value' => $this->t('Open Frontify'),
      '#attributes' => [
        'class' => ['frontify-asset-insert-button', 'btn', 'btn-primary'],
        'id' => ['frontify-asset-insert-button'],
      ],
      '#attached' => [
        'library' => [
          (version_compare(Drupal::VERSION, '9.2.0') >= 0) ?
            'frontify/frontify_drupal_once' :
            'frontify/frontify_drupal_jquery',
        ],
        'drupalSettings' => [
          'Frontify' =>
            [
              'api_url' => $config->get('frontify_api_url'),
              'debug_mode' => $config->get('debug_mode'),
            ],
        ],
      ],
      '#limit_validation_errors' => [],
    ];

    $element['alt'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Alt text'),
      '#default_value' => $item->alt ?? NULL,
      '#maxlength' => 255,
      '#description' => $this->t(
        'Short description of the image used by screen readers and displayed when the image is not loaded. This is important for accessibility.'
      ),
      '#attributes' => [
        'class' => ['frontify-asset-alt-text'],
      ],
    ];

    $element += [
      '#type' => 'fieldset',
    ];

    return $element;
  }

}
