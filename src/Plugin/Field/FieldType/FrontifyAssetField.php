<?php

namespace Drupal\frontify\Plugin\Field\FieldType;

use Drupal\Core\Field\Annotation\FieldType;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\Url;

/**
 * Plugin implementation of the 'frontify_asset_field' field type.
 *
 * @FieldType(
 *   id = "frontify_asset_field",
 *   label = @Translation("Frontify Asset"),
 *   description = @Translation("This field is used for frontify asset
 *   integration"), category = @Translation("General"), default_widget =
 *   "frontify_asset_field_widget", default_formatter =
 *   "frontify_asset_field_formatter", constraints = {"FrontifyAssetLink" = {}}
 * )
 */
class FrontifyAssetField extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition): array {
    $properties['uri'] = DataDefinition::create('uri')
      ->setLabel(t('URI'));
    $properties['alt'] = DataDefinition::create('string')
      ->setLabel(t('Img alt text'));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition): array {
    return [
      'columns' => [
        'uri' => [
          'description' => 'The URI of the link.',
          'type' => 'varchar',
          'length' => 2048,
        ],
        'alt' => [
          'description' => 'Alternative text.',
          'type' => 'varchar',
          'length' => 255,
        ],
      ],
      'indexes' => [
        'uri' => [['uri', 30]],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function mainPropertyName(): string {
    return 'uri';
  }

  /**
   * {@inheritdoc}
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   */
  public function isEmpty(): bool {
    $value = $this->get('uri')->getValue();
    return $value === NULL || $value === '';
  }

  public function isExternal(): bool {
    return $this->getUrl()->isExternal();
  }

  public function getUrl(): Url {
    return Url::fromUri($this->uri);
  }

}
