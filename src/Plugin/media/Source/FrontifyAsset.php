<?php

namespace Drupal\frontify\Plugin\media\Source;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Field\FieldConfigInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\media\Annotation\MediaSource;
use Drupal\media\MediaSourceBase;
use Drupal\media\MediaSourceFieldConstraintsInterface;
use Drupal\media\MediaTypeInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides media type plugin for frontify asset.
 *
 * @MediaSource(
 *   id = "frontify_asset",
 *   label = @Translation("Frontify Asset"),
 *   description = @Translation("Use frontify assets or reusable media."),
 *   allowed_field_types = {"frontify_asset_field"},
 * )
 */
class FrontifyAsset extends MediaSourceBase implements MediaSourceFieldConstraintsInterface {

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ): MediaSourceBase|FrontifyAsset|ContainerFactoryPluginInterface|static {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('entity_field.manager'),
      $container->get('plugin.manager.field.field_type'),
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getMetadataAttributes(): ?array {
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getSourceFieldConstraints(): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function createSourceField(MediaTypeInterface $type): FieldConfigInterface|EntityInterface {
    return parent::createSourceField($type)->set('label', 'Frontify Asset Url');
  }

}
