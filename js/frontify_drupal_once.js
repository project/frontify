/**
 * @file
 * Provides additional javascript for managing the frontify drupal.
 */
(function (Drupal, once) {
  Drupal.behaviors.FrontifyField = {
    attach() {
      once(
        'open-frontify-finder',
        'input.frontify-asset-insert-button',
      ).forEach(el => {
        const $frontify = Drupal.frontify;
        $frontify.handleFinder(el);
      });
    },
  };
})(Drupal, once);
