/**
 * @file
 * Provides additional javascript for managing the frontify drupal (jQuery way).
 */
(function ($) {
  Drupal.behaviors.FrontifyFieldjQuery = {
    attach() {
      $('input.frontify-asset-insert-button')
        .once('open-frontify-finder')
        .each(function () {
          const $frontify = Drupal.frontify;
          $frontify.handleFinder(this);
        });
    },
  };
})(jQuery);
